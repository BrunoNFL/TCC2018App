//
//  celulaProdutos.m
//  ChaveFlex2
//
//  Created by Cesar on 14/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import "CelulaProdutos.h"

@implementation CelulaProdutos

- (instancetype)initWithFrame:(CGRect)rect{
    self = [super initWithFrame:rect];
    if (self) {
        // Add customisation here...
        
        self.backgroundColor = [UIColor whiteColor];
        [self.layer setCornerRadius:25.0f];
        [self.layer setBorderWidth:4.0f];
        [self.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.layer setShadowOpacity:0.15];
        [self.layer setShadowRadius:4.0];
        [self.layer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
        
        self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.origin.x + 7.5, self.bounds.origin.y + 7.5, rect.size.width - 15, 123.5)];
        self.imgView.clipsToBounds = YES;
        self.imgView.layer.cornerRadius = 20.0f;
        self.imgView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview: self.imgView];
        
        
        self.precoDoProduto = [[UILabel alloc] init];
        self.precoDoProduto.font = [UIFont systemFontOfSize:20.0f];
        self.precoDoProduto.textColor = [UIColor blackColor];//[UIColor colorWithRed:0.0 green:203.0/255.0 blue:1.0 alpha:1.0];
        self.precoDoProduto.text = @"00,00";
        self.precoDoProduto.frame = CGRectMake(self.bounds.size.width - self.precoDoProduto.intrinsicContentSize.width - 15, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height, self.precoDoProduto.intrinsicContentSize.width, self.precoDoProduto.intrinsicContentSize.height);
        [self.contentView addSubview: self.precoDoProduto];
        
        
        self.simboloDaMoeda = [[UILabel alloc] init];
        self.simboloDaMoeda.text = @"R$";
        self.simboloDaMoeda.textColor = [UIColor darkGrayColor];
        self.simboloDaMoeda.font = [UIFont systemFontOfSize:13.0f];
        //simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - simboloDaMoeda.intrinsicContentSize.width, - 6.5 - simboloDaMoeda.intrinsicContentSize.height + self.precoDoProduto.frame.origin.y + self.precoDoProduto.intrinsicContentSize.height, simboloDaMoeda.intrinsicContentSize.width, simboloDaMoeda.intrinsicContentSize.height);
        self.simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - self.simboloDaMoeda.intrinsicContentSize.width, self.precoDoProduto.frame.origin.y + 6.5, self.simboloDaMoeda.intrinsicContentSize.width, self.simboloDaMoeda.intrinsicContentSize.height);
        [self.contentView addSubview: self.simboloDaMoeda];
        
        self.quantidadeDoProduto = [[UILabel alloc]init];
        self.quantidadeDoProduto.font = [UIFont systemFontOfSize:20.0f];
        self.quantidadeDoProduto.textColor = [UIColor blackColor];
        self.quantidadeDoProduto.text = @"1";
        self.quantidadeDoProduto.textAlignment = NSTextAlignmentCenter;
        [self.quantidadeDoProduto.layer setBorderColor: [UIColor whiteColor].CGColor];
        self.quantidadeDoProduto.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height, self.quantidadeDoProduto.intrinsicContentSize.width, self.quantidadeDoProduto.intrinsicContentSize.height);
        [self.contentView addSubview: self.quantidadeDoProduto];
        
        self.nomeDoProduto = [[UILabel alloc] init];
        self.nomeDoProduto.textColor = [UIColor darkGrayColor];
        self.nomeDoProduto.text = @"Chaveiro personalizado";
        //self.nomeDoProduto.frame = CGRectMake(self.bounds.origin.x + 7.5, self.bounds.origin.y + 32 + rect.size.height/2, self.nomeDoProduto.intrinsicContentSize.width, self.nomeDoProduto.intrinsicContentSize.height);
        self.nomeDoProduto.frame = CGRectMake(self.quantidadeDoProduto.frame.origin.x, self.imgView.frame.origin.y + self.imgView.frame.size.height, self.nomeDoProduto.intrinsicContentSize.width, self.nomeDoProduto.intrinsicContentSize.height);
        [self.contentView addSubview: self.nomeDoProduto];
        
        
        etiquetaDePreco = [[UILabel alloc] init];
        etiquetaDePreco.text = @"Preço";
        etiquetaDePreco.font = [UIFont systemFontOfSize:10.0f];
        etiquetaDePreco.textColor = [UIColor grayColor];
        etiquetaDePreco.frame = CGRectMake(/*self.bounds.size.width - etiquetaDePreco.intrinsicContentSize.width - 15*/self.simboloDaMoeda.frame.origin.x, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height - etiquetaDePreco.intrinsicContentSize.height, etiquetaDePreco.intrinsicContentSize.width, etiquetaDePreco.intrinsicContentSize.height);
        [self.contentView addSubview: etiquetaDePreco];
        
        
        etiquetaDeQuantidade = [[UILabel alloc] init];
        etiquetaDeQuantidade.text = @"Quant.";
        etiquetaDeQuantidade.font = [UIFont systemFontOfSize:10.0f];
        etiquetaDeQuantidade.textColor = [UIColor grayColor];
        etiquetaDeQuantidade.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height - etiquetaDeQuantidade.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, etiquetaDeQuantidade.intrinsicContentSize.height);
        [self.contentView addSubview: etiquetaDeQuantidade];
        
        self.quantidadeDoProduto.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, self.quantidadeDoProduto.intrinsicContentSize.height);
        
        //NSDictionary *views = NSDictionaryOfVariableBindings(_imageView);
        
        /*[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imageView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:views]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[_imageView]|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:views]];*/
    }
    return self;
    
}

@end