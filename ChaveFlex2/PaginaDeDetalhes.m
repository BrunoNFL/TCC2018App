//
//  paginaDeDetalhes.m
//  ChaveFlex2
//
//  Created by Cesar on 21/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import "PaginaDeDetalhes.h"
#import "ViewController.h"

@implementation PaginaDeDetalhes

- (instancetype)initWithFrame:(CGRect)rect{
    self = [super initWithFrame:rect];
    if (self) {
        // Add customisation here...
        
        frameAntigo = rect;
        
        //self.backgroundColor = [UIColor whiteColor];
        self.effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        [self.layer setCornerRadius:25.0f];
        [self.layer setBorderColor:[UIColor clearColor].CGColor];
        [self.layer setBorderWidth:4.0f];
        [self.layer setShadowRadius:4.0];
        [self.layer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
        self.clipsToBounds = true;
        
        self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.origin.x + 7.5, self.bounds.origin.y + 7.5, rect.size.width - 15, 123.5)];
        self.imgView.clipsToBounds = YES;
        self.imgView.layer.cornerRadius = 20.0f;
        self.imgView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview: self.imgView];
        
        
        self.precoDoProduto = [[UILabel alloc] init];
        self.precoDoProduto.font = [UIFont systemFontOfSize:20.0f];
        self.precoDoProduto.textColor = [UIColor blackColor];//[UIColor colorWithRed:0.0 green:203.0/255.0 blue:1.0 alpha:1.0];
        self.precoDoProduto.text = @"00,00";
        self.precoDoProduto.frame = CGRectMake(self.bounds.size.width - self.precoDoProduto.intrinsicContentSize.width - 15, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height, self.precoDoProduto.intrinsicContentSize.width, self.precoDoProduto.intrinsicContentSize.height);
        [self.contentView addSubview: self.precoDoProduto];
        
        
        simboloDaMoeda = [[UILabel alloc] init];
        simboloDaMoeda.text = @"R$";
        simboloDaMoeda.textColor = [UIColor darkGrayColor];
        simboloDaMoeda.font = [UIFont systemFontOfSize:13.0f];
        //simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - simboloDaMoeda.intrinsicContentSize.width, - 6.5 - simboloDaMoeda.intrinsicContentSize.height + self.precoDoProduto.frame.origin.y + self.precoDoProduto.intrinsicContentSize.height, simboloDaMoeda.intrinsicContentSize.width, simboloDaMoeda.intrinsicContentSize.height);
        simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - simboloDaMoeda.intrinsicContentSize.width, self.precoDoProduto.frame.origin.y + 6.5, simboloDaMoeda.intrinsicContentSize.width, simboloDaMoeda.intrinsicContentSize.height);
        [self.contentView addSubview: simboloDaMoeda];
        
        self.quantidadeDoProduto = [[UILabel alloc]init];
        self.quantidadeDoProduto.font = [UIFont systemFontOfSize:20.0f];
        self.quantidadeDoProduto.textColor = [UIColor blackColor];
        self.quantidadeDoProduto.text = @"1";
        self.quantidadeDoProduto.textAlignment = NSTextAlignmentCenter;
        self.quantidadeDoProduto.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height, self.quantidadeDoProduto.intrinsicContentSize.width, self.quantidadeDoProduto.intrinsicContentSize.height);
        [self.contentView addSubview: self.quantidadeDoProduto];
        
        self.nomeDoProduto = [[UILabel alloc] init];
        self.nomeDoProduto.textColor = [UIColor darkGrayColor];
        self.nomeDoProduto.text = @"Chaveiro personalizado";
        //self.nomeDoProduto.frame = CGRectMake(self.bounds.origin.x + 7.5, self.bounds.origin.y + 32 + rect.size.height/2, self.nomeDoProduto.intrinsicContentSize.width, self.nomeDoProduto.intrinsicContentSize.height);
        self.nomeDoProduto.frame = CGRectMake(self.quantidadeDoProduto.frame.origin.x, self.imgView.frame.origin.y + self.imgView.frame.size.height, self.nomeDoProduto.intrinsicContentSize.width, self.nomeDoProduto.intrinsicContentSize.height);
        [self.contentView addSubview: self.nomeDoProduto];
        
        
        etiquetaDePreco = [[UILabel alloc] init];
        etiquetaDePreco.text = @"Preço";
        etiquetaDePreco.font = [UIFont systemFontOfSize:10.0f];
        etiquetaDePreco.textColor = [UIColor grayColor];
        etiquetaDePreco.frame = CGRectMake(/*self.bounds.size.width - etiquetaDePreco.intrinsicContentSize.width - 15*/simboloDaMoeda.frame.origin.x, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height - etiquetaDePreco.intrinsicContentSize.height, etiquetaDePreco.intrinsicContentSize.width, etiquetaDePreco.intrinsicContentSize.height);
        [self.contentView addSubview: etiquetaDePreco];
        
        
        etiquetaDeQuantidade = [[UILabel alloc] init];
        etiquetaDeQuantidade.text = @"Quant.";
        etiquetaDeQuantidade.font = [UIFont systemFontOfSize:10.0f];
        etiquetaDeQuantidade.textColor = [UIColor grayColor];
        etiquetaDeQuantidade.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height - etiquetaDeQuantidade.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, etiquetaDeQuantidade.intrinsicContentSize.height);
        self.quantidadeDoProduto.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, self.quantidadeDoProduto.intrinsicContentSize.height);
        [self.contentView addSubview: etiquetaDeQuantidade];
        
        //NSDictionary *views = NSDictionaryOfVariableBindings(_imageView);
        
        /*[self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_imageView]|"
         options:0
         metrics:nil
         views:views]];
         
         [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[_imageView]|"
         options:0
         metrics:nil
         views:views]];*/
    }
    return self;
    
}

-(void)atualizarTamanhosParaNovoFrame:(CGRect)rect {
    
    self.precoDoProduto.font        =   [UIFont systemFontOfSize:self.precoDoProduto.font.pointSize * 2];
    self.quantidadeDoProduto.font   =   [UIFont systemFontOfSize:self.quantidadeDoProduto.font.pointSize * 2];
    simboloDaMoeda.font             =   [UIFont systemFontOfSize:simboloDaMoeda.font.pointSize * 2];
    self.nomeDoProduto.font         =   [UIFont systemFontOfSize:self.nomeDoProduto.font.pointSize * 2];
    etiquetaDePreco.font            =   [UIFont systemFontOfSize:etiquetaDePreco.font.pointSize * 2];
    etiquetaDeQuantidade.font       =   [UIFont systemFontOfSize:etiquetaDeQuantidade.font.pointSize * 2];
    
    self.imgView.frame = CGRectMake(self.bounds.origin.x + 7.5, self.bounds.origin.y + 7.5, rect.size.width - 15, 123.5 * rect.size.height / frameAntigo.size.height);
    
    self.precoDoProduto.frame = CGRectMake(self.bounds.size.width - self.precoDoProduto.intrinsicContentSize.width - 15, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height, self.precoDoProduto.intrinsicContentSize.width, self.precoDoProduto.intrinsicContentSize.height);
    
    simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - simboloDaMoeda.intrinsicContentSize.width, self.precoDoProduto.frame.origin.y + 6.5, simboloDaMoeda.intrinsicContentSize.width, simboloDaMoeda.intrinsicContentSize.height);
    
    self.quantidadeDoProduto.frame = CGRectMake(self.bounds.origin.x + 65, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, self.quantidadeDoProduto.intrinsicContentSize.height);
    
    self.nomeDoProduto.frame = CGRectMake(self.quantidadeDoProduto.frame.origin.x - 50, self.imgView.frame.origin.y + self.imgView.frame.size.height, self.nomeDoProduto.intrinsicContentSize.width, self.nomeDoProduto.intrinsicContentSize.height);
    
    etiquetaDePreco.frame = CGRectMake(simboloDaMoeda.frame.origin.x, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height - etiquetaDePreco.intrinsicContentSize.height, etiquetaDePreco.intrinsicContentSize.width, etiquetaDePreco.intrinsicContentSize.height);
    
    etiquetaDeQuantidade.frame = CGRectMake(self.bounds.origin.x + 65, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height - etiquetaDeQuantidade.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, etiquetaDeQuantidade.intrinsicContentSize.height);
    
    self.quantidadeDoProduto.layer.borderColor  = [UIColor darkGrayColor].CGColor;
    self.quantidadeDoProduto.layer.borderWidth  = 4.0f;
    self.quantidadeDoProduto.layer.cornerRadius = 10.0f;
    
    botaoMenos = [[UIButton alloc] initWithFrame: self.quantidadeDoProduto.frame];
    botaoMais  = [[UIButton alloc] initWithFrame: self.quantidadeDoProduto.frame];
    
    [botaoMenos setTitle:@"-" forState:UIControlStateNormal];
    [botaoMenos addTarget:self action:@selector(botaoMenosPressionado) forControlEvents: UIControlEventTouchUpInside];
    [botaoMenos setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [botaoMenos setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    botaoMenos.titleLabel.font = [UIFont systemFontOfSize:self.quantidadeDoProduto.font.pointSize];
    botaoMenos.frame = CGRectMake(botaoMenos.frame.origin.x - botaoMenos.intrinsicContentSize.width, botaoMenos.frame.origin.y - 10, botaoMenos.intrinsicContentSize.width, botaoMenos.intrinsicContentSize.height);
    
    [botaoMais setTitle:@"+" forState:UIControlStateNormal];
    [botaoMais addTarget:self action:@selector(botaoMaisPressionado) forControlEvents: UIControlEventTouchUpInside];
    [botaoMais setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [botaoMais setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    botaoMais.titleLabel.font = [UIFont systemFontOfSize:self.quantidadeDoProduto.font.pointSize];
    botaoMais.frame = CGRectMake(botaoMais.frame.origin.x + botaoMais.frame.size.width /*+ botaoMais.intrinsicContentSize.width*/, botaoMais.frame.origin.y - 10, botaoMais.intrinsicContentSize.width, botaoMais.intrinsicContentSize.height);
    
    precoOriginal = self.precoDoProduto.text;
    
    [self.contentView addSubview:botaoMenos];
    [self.contentView addSubview:botaoMais];
}

-(void)atualizarTamanhosParaFrameAntigo {
    CGRect rect = frameAntigo;
    
    self.precoDoProduto.font        =   [UIFont systemFontOfSize:self.precoDoProduto.font.pointSize / 2];
    self.quantidadeDoProduto.font   =   [UIFont systemFontOfSize:self.quantidadeDoProduto.font.pointSize / 2];
    simboloDaMoeda.font             =   [UIFont systemFontOfSize:simboloDaMoeda.font.pointSize / 2];
    self.nomeDoProduto.font         =   [UIFont systemFontOfSize:self.nomeDoProduto.font.pointSize / 2];
    etiquetaDePreco.font            =   [UIFont systemFontOfSize:etiquetaDePreco.font.pointSize / 2];
    etiquetaDeQuantidade.font       =   [UIFont systemFontOfSize:etiquetaDeQuantidade.font.pointSize / 2];
    
    self.frame = rect;
    
    self.imgView.frame = CGRectMake(self.bounds.origin.x + 7.5, self.bounds.origin.y + 7.5, rect.size.width - 15, 123.5 * rect.size.height / frameAntigo.size.height);
    
    self.precoDoProduto.frame = CGRectMake(self.bounds.size.width - self.precoDoProduto.intrinsicContentSize.width - 15, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height, self.precoDoProduto.intrinsicContentSize.width, self.precoDoProduto.intrinsicContentSize.height);
    
    simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - simboloDaMoeda.intrinsicContentSize.width, self.precoDoProduto.frame.origin.y + 6.5, simboloDaMoeda.intrinsicContentSize.width, simboloDaMoeda.intrinsicContentSize.height);
    
    self.quantidadeDoProduto.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, self.quantidadeDoProduto.intrinsicContentSize.height);
    
    self.nomeDoProduto.frame = CGRectMake(self.quantidadeDoProduto.frame.origin.x, self.imgView.frame.origin.y + self.imgView.frame.size.height, self.nomeDoProduto.intrinsicContentSize.width, self.nomeDoProduto.intrinsicContentSize.height);
    
    etiquetaDePreco.frame = CGRectMake(/*self.bounds.size.width - etiquetaDePreco.intrinsicContentSize.width - 15*/simboloDaMoeda.frame.origin.x, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height - etiquetaDePreco.intrinsicContentSize.height, etiquetaDePreco.intrinsicContentSize.width, etiquetaDePreco.intrinsicContentSize.height);
    
    etiquetaDeQuantidade.frame = CGRectMake(self.bounds.origin.x + 15, self.bounds.size.height - 10 - self.quantidadeDoProduto.intrinsicContentSize.height - etiquetaDeQuantidade.intrinsicContentSize.height, etiquetaDeQuantidade.intrinsicContentSize.width, etiquetaDeQuantidade.intrinsicContentSize.height);
    
    self.quantidadeDoProduto.layer.borderColor  = [UIColor clearColor].CGColor;
    self.quantidadeDoProduto.layer.borderWidth  = 4.0f;
    self.quantidadeDoProduto.layer.cornerRadius = 10.0f;
    
}

-(void)botaoMenosPressionado{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView transitionWithView:botaoMenos
                          duration:0.05
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ botaoMenos.highlighted = YES; }
                        completion:^(BOOL finished){[UIView transitionWithView:botaoMenos
                                                                      duration:0.05
                                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                                    animations:^{ botaoMenos.highlighted = NO; }
                                                                    completion:nil];
                        }
         ];
    });
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    if ([[f numberFromString:self.quantidadeDoProduto.text] intValue] > 1) {
        self.quantidadeDoProduto.text = [NSString stringWithFormat:@"%d", [[f numberFromString:self.quantidadeDoProduto.text] intValue] - 1];
        self.precoDoProduto.text = [NSString stringWithFormat:@"%.02f", [[f numberFromString:self.quantidadeDoProduto.text] floatValue] * [[f numberFromString:precoOriginal] floatValue]];
        self.precoDoProduto.frame = CGRectMake(self.bounds.size.width - self.precoDoProduto.intrinsicContentSize.width - 15, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height, self.precoDoProduto.intrinsicContentSize.width, self.precoDoProduto.intrinsicContentSize.height);
        simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - simboloDaMoeda.intrinsicContentSize.width, self.precoDoProduto.frame.origin.y + 6.5, simboloDaMoeda.intrinsicContentSize.width, simboloDaMoeda.intrinsicContentSize.height);
    }
}
-(void)botaoMaisPressionado{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView transitionWithView:botaoMais
                          duration:0.05
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ botaoMais.highlighted = YES; }
                        completion:^(BOOL finished){[UIView transitionWithView:botaoMais
                                                                      duration:0.05
                                                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                                                    animations:^{ botaoMais.highlighted = NO; }
                                                                    completion:nil];
                        }
         ];
    });
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    if ([[f numberFromString:self.quantidadeDoProduto.text] intValue] >= 1) {
        self.quantidadeDoProduto.text = [NSString stringWithFormat:@"%d", [[f numberFromString:self.quantidadeDoProduto.text] intValue] + 1];
        self.precoDoProduto.text = [NSString stringWithFormat:@"%.02f", [[f numberFromString:self.quantidadeDoProduto.text] floatValue] * [[f numberFromString:precoOriginal] floatValue]];
        self.precoDoProduto.frame = CGRectMake(self.bounds.size.width - self.precoDoProduto.intrinsicContentSize.width - 15, self.bounds.size.height - 10 - self.precoDoProduto.intrinsicContentSize.height, self.precoDoProduto.intrinsicContentSize.width, self.precoDoProduto.intrinsicContentSize.height);
        simboloDaMoeda.frame = CGRectMake(self.precoDoProduto.frame.origin.x - simboloDaMoeda.intrinsicContentSize.width, self.precoDoProduto.frame.origin.y + 6.5, simboloDaMoeda.intrinsicContentSize.width, simboloDaMoeda.intrinsicContentSize.height);
    }
    
}
@end
