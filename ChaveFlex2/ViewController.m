//
//  ViewController.m
//  ChaveFlex2
//
//  Created by Bruno Cesar on 01/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self criarCollectionView];
    [self.view sendSubviewToBack: botaoPedido];
    [self.view sendSubviewToBack: self.collectionView];
    celulaSelecionada = [[NSMutableArray alloc] init];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    self.navigationController.navigationBar.layer.borderColor   = [[UIColor clearColor] CGColor];
    self.navigationController.navigationBar.layer.borderWidth   = 2;
    self.navigationController.navigationBar.layer.shadowColor   = [[UIColor blackColor] CGColor];
    self.navigationController.navigationBar.layer.shadowOffset  = CGSizeMake(1.0f, 1.0f);
    self.navigationController.navigationBar.layer.shadowRadius  = 4.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.5f;
    self.navigationController.navigationBar.layer.masksToBounds = NO;
    
    botaoPedido.layer.borderColor   = [[UIColor clearColor] CGColor];
    botaoPedido.layer.borderWidth   = 2;
    botaoPedido.layer.shadowColor   = [[UIColor blackColor] CGColor];
    botaoPedido.layer.shadowOffset  = CGSizeMake(1.0f, 1.0f);
    botaoPedido.layer.shadowRadius  = 4.0f;
    botaoPedido.layer.shadowOpacity = 0.5f;
    botaoPedido.layer.masksToBounds = NO;
    
    //[self.view bringSubviewToFront:botaoPedido];
    //[self.view bringSubviewToFront:barraInferior];

    imagensDeProduto = @[@"carro1.jpg", @"carro2.jpg", @"carro3.jpg", @"carro4.jpg", @"carro5.jpg", @"carro6.jpg", @"carro7.jpg", @"carro8.jpg", @"carro9.jpg", @"carro10.jpg"];
    nomesDeProduto   = @[@"Chaveiro 1", @"Chaveiro 2", @"Chaveiro 3", @"Chaveiro 4", @"Chaveiro 5", @"Chaveiro 6", @"Chaveiro 7", @"Chaveiro 8", @"Chaveiro 9", @"Chaveiro 10"];
    precosDeProduto  = @[@"23,50", @"21,30", @"22,00", @"19,99", @"20,50", @"25,50", @"24,00", @"25,00", @"23,00", @"24,50"];
    
    status.textColor = [UIColor redColor];
    
    rodinha.hidesWhenStopped = YES;
    
    Transmissor = [[TransmissorDeArquivos alloc] init];
    
    [self colocarItensNaPaginaDeScroll];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle { return UIStatusBarStyleLightContent; }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)BotãoPedido:(id)sender {
    
    UIAlertController *confimacao=[UIAlertController alertControllerWithTitle:@"Teste"
                                                                 message:@"Fazer pedido agora?"
                                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *botaoSim = [UIAlertAction actionWithTitle:@"Sim"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
    {
        NSLog(@"Sim");
        
        status.textColor = [UIColor blackColor];
        
        [NSThread detachNewThreadSelector: @selector(logicaPrincipal) toTarget: self withObject: nil];
        
    }];
    
    UIAlertAction *botaoNao = [UIAlertAction actionWithTitle:@"Não"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
    {
        NSLog(@"Não");
    }];
    
    [confimacao addAction:botaoSim];
    [confimacao addAction:botaoNao];
    
    [self presentViewController:confimacao animated:YES completion:nil];
    
}
- (void)criarCollectionView{
    [super viewDidLoad];
    
    layout = [[UICollectionViewFlowLayout alloc] init];
    //self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-170) collectionViewLayout:layout];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-100) collectionViewLayout:layout];
    [self.collectionView setContentInset:UIEdgeInsetsMake(20, 10, 20, 10)];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    
    [self.collectionView registerClass:[CelulaProdutos class] forCellWithReuseIdentifier:@"celulaProdutos"];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
    
}

- (void) orientationChanged:(NSNotification *)note{
    [self.collectionView.collectionViewLayout invalidateLayout];
    UIDevice * device = [UIDevice currentDevice];
    [UIView animateWithDuration:0.3
                     animations:^{
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            self.collectionView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-100);
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            self.collectionView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-100);
            break;
        case UIDeviceOrientationLandscapeLeft:
            self.collectionView.frame = CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height-150);
            break;
            
        case UIDeviceOrientationLandscapeRight:
            self.collectionView.frame = CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height-150);
            break;
        default:
            break;
    };
    /*[self.collectionView reloadData]*/;}
                     completion:nil];
}


- (void)colocarItensNaPaginaDeScroll{
    /*UILabel *chamada = [[UILabel alloc]initWithFrame:CGRectMake(10, -20, 600, 40)];
    [chamada setBackgroundColor:[UIColor clearColor]];
    [chamada setTextColor:[UIColor grayColor]];
    [chamada setText:@"Preencha suas opções de pedido:"];
    [chamada setFont:[UIFont systemFontOfSize:15]];
    [paginaDeScroll addSubview:chamada];*/
}

- (void)logicaPrincipal{
    [rodinha performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];
    [status performSelectorOnMainThread:@selector(setTextColor:) withObject:[UIColor redColor] waitUntilDone:NO];
    
    if ([Transmissor conectarAoServidor]){
        
        [status performSelectorOnMainThread:@selector(setTextColor:) withObject:[UIColor yellowColor] waitUntilDone:NO];
        
        if([Transmissor autenticarConexao]){
            
            status.textColor = [UIColor greenColor];
            [rodinha performSelectorOnMainThread:@selector(stopAnimating) withObject:nil waitUntilDone:NO];
            
            [Transmissor conexaoSFTP];
            NSNumber *numeroDoPedido = [Transmissor ordemDoProximoPedido];
            [[CriadorPlist alloc] criarPlistDePedido:YES comNumero:numeroDoPedido comArquivo:@"" comNome:@"Bruno Cesar"];
            if([Transmissor transmitirOArquivo:[NSString stringWithFormat:@"pedido_%d", [numeroDoPedido intValue]]]){
                [self performSelectorOnMainThread:@selector(pedidoRealizado) withObject:nil waitUntilDone:NO];
            }
            else [self performSelectorOnMainThread:@selector(conexaoFalhou:) withObject:[NSNumber numberWithInt:3] waitUntilDone:NO];
        }
        else [self performSelectorOnMainThread:@selector(conexaoFalhou:) withObject:[NSNumber numberWithInt:2] waitUntilDone:NO];
    }
    else [self performSelectorOnMainThread:@selector(conexaoFalhou:) withObject:[NSNumber numberWithInt:1] waitUntilDone:NO];
}

- (void)conexaoFalhou:(NSNumber *)codigo{
    int codigoInt = [codigo intValue];
    UIAlertController *alerta;
    if (codigoInt == 1){
        status.textColor = [UIColor redColor];
        [rodinha stopAnimating];
        alerta=[UIAlertController alertControllerWithTitle:@"Status do Pedido"
                                                                       message:@"A conexão com o servidor falhou!\n\nTente novamente mais tarde."
                                                                preferredStyle:UIAlertControllerStyleAlert];
    }
    if (codigoInt == 2) {
        status.textColor = [UIColor yellowColor];
        [rodinha stopAnimating];
        alerta=[UIAlertController alertControllerWithTitle:@"Status do Pedido"
                                                                       message:@"A autenticação com o servidor falhou!\n\n Tente novamente."
                                                                preferredStyle:UIAlertControllerStyleAlert];
    }
    if (codigoInt == 3) {
        status.textColor = [UIColor yellowColor];
        [rodinha stopAnimating];
        alerta=[UIAlertController alertControllerWithTitle:@"Status do Pedido"
                                                                       message:@"Pedido não enviado!\n\nNão conseguimos transmitir seu pedido, tente novamente mais tarde."
                                                                preferredStyle:UIAlertControllerStyleAlert];
    }
    UIAlertAction *botaoOk = [UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action)
                              {
                                  NSLog(@"OK");
                                  
                              }];
    [alerta addAction:botaoOk];
    [self presentViewController:alerta animated:YES completion:nil];
}

-(void)pedidoRealizado{
    UIAlertController *sucesso=[UIAlertController alertControllerWithTitle:@"Status do Pedido"
                                                                      message:@"Pedido realizado com sucesso!\n\nVocê será avisado sobre o status da produção do seu produto a cada etapa."
                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *botaoOk = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   NSLog(@"OK");
                                   
                               }];
    [sucesso addAction:botaoOk];
    
    [self presentViewController:sucesso animated:YES completion:nil];
}


//UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CelulaProdutos *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"celulaProdutos" forIndexPath:indexPath];
    
    if ([celulaSelecionada containsObject: indexPath]){
        [cell.layer setBorderColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0].CGColor];
    }
     else{
        [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
        cell.imgView.contentMode = UIViewContentModeScaleToFill;
        cell.imgView.image = [UIImage imageNamed:[imagensDeProduto objectAtIndex:indexPath.row]];
        
        cell.nomeDoProduto.text = [nomesDeProduto objectAtIndex:indexPath.row];
        //cell.nomeDoProduto.frame = CGRectMake(cell.quantidadeDoProduto.frame.origin.x, cell.bounds.origin.y + 32 + cell.frame.size.height/2, cell.nomeDoProduto.intrinsicContentSize.width, cell.nomeDoProduto.intrinsicContentSize.height);
         
        cell.precoDoProduto.text = [precosDeProduto objectAtIndex:indexPath.row];
        cell.precoDoProduto.frame = CGRectMake(cell.bounds.size.width - cell.precoDoProduto.intrinsicContentSize.width - 15, cell.bounds.size.height - 10 - cell.precoDoProduto.intrinsicContentSize.height, cell.precoDoProduto.intrinsicContentSize.width, cell.precoDoProduto.intrinsicContentSize.height);
         
        cell.quantidadeDoProduto.text = @"1";
        cell.quantidadeDoProduto.frame = CGRectMake(cell.bounds.origin.x + 15, cell.bounds.size.height - 10 - cell.quantidadeDoProduto.intrinsicContentSize.height, cell.quantidadeDoProduto.intrinsicContentSize.width, cell.quantidadeDoProduto.intrinsicContentSize.height);
     }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIDevice * device = [UIDevice currentDevice];
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            return CGSizeMake(self.view.frame.size.width/2 - 15, self.view.frame.size.width/2);
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            return CGSizeMake(self.view.frame.size.width/2 - 15, self.view.frame.size.width/2);
            break;
        case UIDeviceOrientationLandscapeLeft:
            return CGSizeMake(self.view.frame.size.width/3 - 15, self.view.frame.size.width/3);
            break;
            
        case UIDeviceOrientationLandscapeRight:
            return CGSizeMake(self.view.frame.size.width/3 - 15, self.view.frame.size.width/3);
            break;
        default:
            return CGSizeMake(self.view.frame.size.width/2 - 15, self.view.frame.size.width/2);
            break;
    };
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CelulaProdutos *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    cell.imgView.contentMode = UIViewContentModeScaleToFill;
    cell.imgView.image = [UIImage imageNamed:[imagensDeProduto objectAtIndex:indexPath.row]];
    
    if ([celulaSelecionada containsObject: indexPath]){
        cell.layer.borderColor = [UIColor whiteColor].CGColor;
        [celulaSelecionada removeObject:indexPath];
        
        cell.nomeDoProduto.text = [nomesDeProduto objectAtIndex:indexPath.row];
        //cell.nomeDoProduto.frame = CGRectMake(cell.quantidadeDoProduto.frame.origin.x, cell.bounds.origin.y + 32 + cell.frame.size.height/2, cell.nomeDoProduto.intrinsicContentSize.width, cell.nomeDoProduto.intrinsicContentSize.height);
        
        cell.precoDoProduto.text = [precosDeProduto objectAtIndex:indexPath.row];
        cell.precoDoProduto.frame = CGRectMake(cell.bounds.size.width - cell.precoDoProduto.intrinsicContentSize.width - 15, cell.bounds.size.height - 10 - cell.precoDoProduto.intrinsicContentSize.height, cell.precoDoProduto.intrinsicContentSize.width, cell.precoDoProduto.intrinsicContentSize.height);
        
        cell.quantidadeDoProduto.text = @"1";
        cell.quantidadeDoProduto.frame = CGRectMake(cell.bounds.origin.x + 15, cell.bounds.size.height - 10 - cell.quantidadeDoProduto.intrinsicContentSize.height, cell.quantidadeDoProduto.intrinsicContentSize.width, cell.quantidadeDoProduto.intrinsicContentSize.height);
    }
    else{
        [self criarPaginaDeDetalhesComDadosDaColecao:collectionView eIndice:indexPath];
        cell.layer.borderColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0].CGColor;
        [celulaSelecionada addObject:indexPath];
    }
    //NSLog([NSString stringWithFormat:@"%@", indexPath]);
}

-(void)criarPaginaDeDetalhesComDadosDaColecao:(UICollectionView *)colecao eIndice:(NSIndexPath *)indice{
    CelulaProdutos *celula = [colecao cellForItemAtIndexPath:indice];
    
    UICollectionViewLayoutAttributes *atributos = [colecao layoutAttributesForItemAtIndexPath:indice];
    CGRect frameDaCelula = atributos.frame;
    CGRect frameDaCelulaNoSuperview = [colecao convertRect:frameDaCelula toView:[colecao superview]];

    sombra = [[UIView alloc] initWithFrame: [UIScreen mainScreen].bounds];
    sombra.backgroundColor = [UIColor blackColor];
    sombra.alpha = 0.0;
    
    UITapGestureRecognizer *reconhecerToqueNoFundo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sairDaPaginaDeDetalhes)];
    reconhecerToqueNoFundo.numberOfTapsRequired = 1;
    [sombra addGestureRecognizer:reconhecerToqueNoFundo];
    
    //paginaDeDetalhes = [[PaginaDeDetalhes alloc] initWithFrame: CGRectMake(self.collectionView.frame.origin.x + celula.bounds.origin.x + 10, self.collectionView.frame.origin.y + celula.bounds.origin.y + 20, celula.bounds.size.width, celula.bounds.size.height)];
    paginaDeDetalhes = [[PaginaDeDetalhes alloc] initWithFrame: frameDaCelulaNoSuperview];
    paginaDeDetalhes.imgView.image = celula.imgView.image;
    paginaDeDetalhes.precoDoProduto.text = celula.precoDoProduto.text;
    paginaDeDetalhes.quantidadeDoProduto.text = celula.quantidadeDoProduto.text;
    paginaDeDetalhes.nomeDoProduto.text = celula.nomeDoProduto.text;
    
    [self.view addSubview:sombra];
    [self.view addSubview: paginaDeDetalhes];
    //[self.view sendSubviewToBack: paginaDeDetalhes];
    //[self.view sendSubviewToBack: sombra];
    //[self.view sendSubviewToBack: self.collectionView];
    [self.view bringSubviewToFront:sombra];
    [self.view bringSubviewToFront: paginaDeDetalhes];
    
    celula.hidden = YES;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         celulaAberta = indice;
                         sombra.alpha = 0.5;
                         double newWidth  = paginaDeDetalhes.frame.size.width * 2;
                         double newHeight = paginaDeDetalhes.frame.size.height * 2;
                         
                         paginaDeDetalhes.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2 - newWidth/2, [UIScreen mainScreen].bounds.size.height/2 - newHeight/2, newWidth, newHeight);
                         
                         [paginaDeDetalhes atualizarTamanhosParaNovoFrame: paginaDeDetalhes.frame];
                     }
                     completion:nil];
}

-(void)sairDaPaginaDeDetalhes{
    CelulaProdutos *celula = [self.collectionView cellForItemAtIndexPath:celulaAberta];
    [self.view sendSubviewToBack: sombra];
    [self.view sendSubviewToBack: paginaDeDetalhes];
    [self.view sendSubviewToBack: self.collectionView];
    [UIView animateWithDuration:0.2
                     animations:^{
                         sombra.alpha = 0.0;
                         [paginaDeDetalhes atualizarTamanhosParaFrameAntigo];
                         celula.precoDoProduto.text = paginaDeDetalhes.precoDoProduto.text;
                         celula.quantidadeDoProduto.text = paginaDeDetalhes.quantidadeDoProduto.text;
                         celula.nomeDoProduto.text = paginaDeDetalhes.nomeDoProduto.text;
                     }
                     completion:^(BOOL finished){
                         celula.hidden = NO;
                         [UIView animateWithDuration:0.5
                                          animations:^{
                                              [sombra removeFromSuperview];
                                              [paginaDeDetalhes removeFromSuperview];
                                          }
                                          completion:nil];
                     }];
    celula.precoDoProduto.frame = CGRectMake(celula.bounds.size.width - celula.precoDoProduto.intrinsicContentSize.width - 15, celula.bounds.size.height - 10 - celula.precoDoProduto.intrinsicContentSize.height, celula.precoDoProduto.intrinsicContentSize.width, celula.precoDoProduto.intrinsicContentSize.height);
    celula.simboloDaMoeda.frame = CGRectMake(celula.precoDoProduto.frame.origin.x - celula.simboloDaMoeda.intrinsicContentSize.width, celula.precoDoProduto.frame.origin.y + 6.5, celula.simboloDaMoeda.intrinsicContentSize.width, celula.simboloDaMoeda.intrinsicContentSize.height);
    celula.quantidadeDoProduto.frame = CGRectMake(celula.bounds.origin.x + 15, celula.bounds.size.height - 10 - celula.quantidadeDoProduto.intrinsicContentSize.height, celula.quantidadeDoProduto.intrinsicContentSize.width, celula.quantidadeDoProduto.intrinsicContentSize.height);
}
@end
