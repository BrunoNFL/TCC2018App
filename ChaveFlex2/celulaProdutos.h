//
//  celulaProdutos.h
//  ChaveFlex2
//
//  Created by Cesar on 14/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CelulaProdutos : UICollectionViewCell{
    UILabel *etiquetaDeQuantidade;
    UILabel *etiquetaDePreco;
    //UILabel *simboloDaMoeda;
}

@property (strong, nonatomic) UILabel *nomeDoProduto;
@property (strong, nonatomic) UILabel *precoDoProduto;
@property (strong, nonatomic) UILabel *simboloDaMoeda;
@property (strong, nonatomic) UILabel *quantidadeDoProduto;
@property (strong, nonatomic) UIImageView *imgView;
//- (instancetype)initWithFrame:(CGRect)rect;


@end
