//
//  TransmissorDeArquivos.h
//  ChaveFlex2
//
//  Created by Bruno Cesar on 06/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import <NMSSH/NMSSH.h>
#import <Foundation/Foundation.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "CriadorPlist.h"

@interface TransmissorDeArquivos : NMSSHChannel {
    NMSSHSession *conexao;
    //NSMutableArray      *listaDeNumerosDosArquivos;
    //NSArray      *numerosSortidos;
}
- (BOOL)conectarAoServidor;
- (id)init;
- (BOOL)conexaoSFTP;
- (BOOL)autenticarConexao;
- (BOOL)transmitirOArquivo: (NSString*)nome;
- (NSArray *) verificarPedidosExistentes;
- (NSNumber *) ordemDoProximoPedido;
- (NSString *) descobrirIPLocal;
@end