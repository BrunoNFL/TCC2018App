//
//  main.m
//  ChaveFlex2
//
//  Created by Bruno Cesar on 01/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
