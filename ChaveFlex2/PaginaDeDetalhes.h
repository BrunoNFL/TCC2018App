//
//  paginaDeDetalhes.h
//  ChaveFlex2
//
//  Created by Cesar on 21/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CelulaProdutos.h"

@interface PaginaDeDetalhes : UIVisualEffectView {//UIView {
    UILabel *etiquetaDeQuantidade;
    UILabel *etiquetaDePreco;
    UILabel *simboloDaMoeda;
    UIButton *botaoMenos;
    UIButton *botaoMais;
    CGRect  frameAntigo;
    NSString *precoOriginal;
}

@property (strong, nonatomic) UILabel *nomeDoProduto;
@property (strong, nonatomic) UILabel *precoDoProduto;
@property (strong, nonatomic) UILabel *quantidadeDoProduto;
@property (strong, nonatomic) UIImageView *imgView;

-(void)atualizarTamanhosParaNovoFrame:(CGRect)rect;
-(void)atualizarTamanhosParaFrameAntigo;
-(void)botaoMenosPressionado;
-(void)botaoMaisPressionado;

@end