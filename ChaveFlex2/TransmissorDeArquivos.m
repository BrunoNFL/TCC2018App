//
//  TransmissorDeArquivos.m
//  ChaveFlex2
//
//  Created by Bruno Cesar on 06/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//


#import "TransmissorDeArquivos.h"

@implementation TransmissorDeArquivos

-(id)init{
    self = [super init];
    if (self) {
        NSString *IPLocal = [NSString stringWithFormat:@"%@", [self descobrirIPLocal]];                                                 //Rotina para des-
        NSArray *componentesIP = [IPLocal componentsSeparatedByString:@"."];                                                            //cobrir o IP do
        NSString *IPServidor = [NSString stringWithFormat:@"%@.%@.%@.%@", componentesIP[0], componentesIP[1], componentesIP[2], @"215"];//Servidor :)
        conexao = [[NMSSHSession alloc] initWithHost:IPServidor port: 22 andUsername:@"Cesar"];
    }
    return self;
}

- (NSString *)descobrirIPLocal {
    
    NSString *endereco = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *enderecoTemporario = NULL;
    int sucesso = 0;
    sucesso = getifaddrs(&interfaces);
    if (sucesso == 0) {
        enderecoTemporario = interfaces;
        while(enderecoTemporario != NULL) {
            if(enderecoTemporario->ifa_addr->sa_family == AF_INET) {
                if([[NSString stringWithUTF8String:enderecoTemporario->ifa_name] isEqualToString:@"en0"]) {
                    endereco = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)enderecoTemporario->ifa_addr)->sin_addr)];
                }
            }
            enderecoTemporario = enderecoTemporario->ifa_next;
        }
    }
    freeifaddrs(interfaces);
    return endereco;
}

- (BOOL)conectarAoServidor{
    
    [conexao connect];
    if (conexao.isConnected) {
        
        NSLog(@"Conectado com sucesso!");
        return YES;
    }
    else {
        return NO;
    }
}

-(BOOL)autenticarConexao{
    if (conexao.isConnected) {
        [conexao authenticateByPassword:@"1234"];
    
        if (conexao.isAuthorized) {
        
            NSLog(@"Autenticado com sucesso!");
            
            return YES;
        }
        
        else{
            
            return NO;
            
        }
    }
    else{
        return NO;
    }
}


- (BOOL)conexaoSFTP{
    
    [conexao.sftp connect];
    
    return YES;
}

- (NSArray *) verificarPedidosExistentes{
    
    NSArray *listaDePedidosRemoto = [conexao.sftp contentsOfDirectoryAtPath:@"Servidor/Pedidos"];
    NSArray *listaEmProgressoRemoto = [conexao.sftp contentsOfDirectoryAtPath:@"Servidor/Em_Progresso"];
    NSArray *listaDeConcluidosRemoto = [conexao.sftp contentsOfDirectoryAtPath:@"Servidor/Concluidos"];
    
    NSArray *listaParcialDePedidosJaFeitos = listaDePedidosRemoto?[listaDePedidosRemoto arrayByAddingObjectsFromArray:listaEmProgressoRemoto]:[[NSArray alloc] initWithArray:listaEmProgressoRemoto];
    
    NSArray *listaTotalDePedidosJaFeitos = listaParcialDePedidosJaFeitos?[listaParcialDePedidosJaFeitos arrayByAddingObjectsFromArray:listaDeConcluidosRemoto]:[[NSArray alloc] initWithArray:listaDeConcluidosRemoto];
    
    NSMutableArray *listaDeNumerosDosArquivos = [[NSMutableArray alloc] init];
    
    for (NMSFTPFile *arquivos in listaTotalDePedidosJaFeitos) {
        if(![arquivos.filename  isEqual: @".DS_Store"]){
            NSLog(@"%@", arquivos.filename);
            NSString *nomeBruto = arquivos.filename; //pedido_
            NSString *somenteNumero = [nomeBruto substringFromIndex:7];
            NSNumber *numero = [NSNumber numberWithInteger: [somenteNumero intValue]];
            [listaDeNumerosDosArquivos addObject:numero];
        }
    }
    
    NSSortDescriptor *ordem = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: YES];
    NSArray *numerosSortidos = [listaDeNumerosDosArquivos sortedArrayUsingDescriptors: [NSArray arrayWithObject: ordem]];
    
    return numerosSortidos;
    
}

- (NSNumber *) ordemDoProximoPedido{
    NSArray *todosOsNumeros = [self verificarPedidosExistentes];
    int numeroDoIndice = [[NSNumber numberWithInteger:todosOsNumeros.count] intValue];
    NSNumber *numeroCerto = [NSNumber numberWithInteger: [todosOsNumeros[numeroDoIndice - 1] intValue] + 1];
    
    return numeroCerto;
}

- (BOOL)transmitirOArquivo: (NSString*)nome{
    BOOL sucesso = [conexao.channel uploadFile:[NSString stringWithFormat:@"%@/%@.plist", [[[CriadorPlist alloc] caminhoDaPastaDocumentos] path], nome]
                                            to:[NSString stringWithFormat:@"Servidor/Pedidos/%@.plist", nome]];
    
    if (sucesso) {
        return YES;
    }
    else{
        return NO;
    }
}


@end