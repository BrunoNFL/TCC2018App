//
//  CriadorPlist.h
//  ChaveFlex2
//
//  Created by Bruno Cesar on 06/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransmissorDeArquivos.h"


@interface CriadorPlist : NSObject {
    NSString *caminhoPlist;
    NSString *destinoPlist;
    NSArray  *pedidosExistentes;
    BOOL     *criouPlist;
    NSNumber *numeroDePedidoAnterior;
}

- (void) criarPlistDePedido:(BOOL)pedido comNumero:(NSNumber *)ordemDoPedido comArquivo:(NSString *)arquivo comNome:(NSString *)nomeDoCliente;
- (BOOL) verificarCriaçãoDePlist;
- (NSURL *) caminhoDaPastaDocumentos;
@end

