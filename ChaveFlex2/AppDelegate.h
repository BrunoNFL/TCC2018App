//
//  AppDelegate.h
//  ChaveFlex2
//
//  Created by Bruno Cesar on 01/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

