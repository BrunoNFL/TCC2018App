//
//  ViewController.h
//  ChaveFlex2
//
//  Created by Bruno Cesar on 01/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CriadorPlist.h"
#import "TransmissorDeArquivos.h"
#import "CelulaProdutos.h"
#import "PaginaDeDetalhes.h"

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    //IBOutlet UILabel *texto1;
    TransmissorDeArquivos                   *Transmissor;
    PaginaDeDetalhes                        *paginaDeDetalhes;
    UIView                                  *sombra;
    __weak IBOutlet UILabel                 *status;
    __weak IBOutlet UIActivityIndicatorView *rodinha;
    __weak IBOutlet UIScrollView            *paginaDeScroll;
    __weak IBOutlet UIButton                *botaoPedido;
    __weak IBOutlet UIView                  *barraInferior;
    UICollectionViewFlowLayout              *layout;
    NSMutableArray                          *celulaSelecionada;
    NSIndexPath                             *celulaAberta;
    NSArray                                 *imagensDeProduto;
    NSArray                                 *nomesDeProduto;
    NSArray                                 *precosDeProduto;
}

@property(nonatomic) BOOL hidesWhenStopped;
@property(nonatomic) UIViewAutoresizing redimensionamentoAutomatico;
@property(nonatomic) NSArray *nomes;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIView *telaPrincipal;

- (IBAction)BotãoPedido:(id)sender;
- (void)logicaPrincipal;
- (void)conexaoFalhou:(NSNumber *)codigo;
- (void)colocarItensNaPaginaDeScroll;
- (void)criarCollectionView;
- (void)criarPaginaDeDetalhesComDadosDaColecao:(UICollectionView *)colecao eIndice:(NSIndexPath *)indice;

@end

