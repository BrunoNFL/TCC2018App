//
//  CriadorPlist.m
//  ChaveFlex2
//
//  Created by Bruno Cesar on 03/03/18.
//  Copyright © 2018 TCC2018Automação. All rights reserved.
//

#import "CriadorPlist.h"



@implementation CriadorPlist

- (void) criarPlistDePedido:(BOOL)pedido comNumero:(NSNumber *)ordemDoPedido comArquivo:(NSString *)arquivo comNome:(NSString *)nomeDoCliente{
    
    NSString *caminhoDoArquivo = [NSString stringWithFormat:@"%@/pedido_%d.plist", [[self caminhoDaPastaDocumentos] path], [ordemDoPedido intValue]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:caminhoDoArquivo]) {
        
        NSLog(@"Não precisou criar o arquivo!");
        
    }
    
    else {
        NSMutableDictionary *arquivoDePedido = [NSMutableDictionary dictionaryWithCapacity:3];
        NSDictionary *dicionarioDeCriacao;
        NSString *nome;
        NSArray *pedidos;
        NSError *error = nil;
    
        pedidos = [NSArray arrayWithObjects:[NSString stringWithFormat: @"%@/%@", [[self caminhoDaPastaDocumentos] path], @"arquivo1"],
                                            [NSString stringWithFormat: @"%@/%@", [[self caminhoDaPastaDocumentos] path], @"arquivo2"],
                                            [NSString stringWithFormat: @"%@/%@", [[self caminhoDaPastaDocumentos] path], @"arquivo3"], nil];
        
        nome = nomeDoCliente;
        
        dicionarioDeCriacao = [NSDictionary dictionaryWithObjects:
                                [NSArray arrayWithObjects: nome, pedidos, nil]
                                                forKeys:[NSArray arrayWithObjects:@"Nome", @"Pedidos", nil]];
        
        [arquivoDePedido setObject:dicionarioDeCriacao forKey:@"BrunoCesar"];
    
        
        id plist = [NSPropertyListSerialization dataWithPropertyList:(id)arquivoDePedido
                                                              format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
    
        BOOL sucesso = [plist writeToFile: caminhoDoArquivo atomically:YES];
    
        if (sucesso) {
            NSLog(@"Criou o arquivo com sucesso!!!!");
        }
        
        else{
            NSLog(@"Não criou o arquivo!!!!");
        }
    }
}

- (BOOL) verificarCriaçãoDePlist{
    
    return criouPlist;
}

- (NSURL *) caminhoDaPastaDocumentos
{
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
}

@end